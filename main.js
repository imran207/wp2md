const parser = require('xml2js');
const readline = require('readline');
const ingest = require('./ingest');
const eshodinshikhi = require('./eshodinshikhi');

const convert_to_md = false;

const writer = convert_to_md? require('./writer-md') : require('./writer-json');

function on_json_create(err, json) {
	if (!err) {
		const channel = ingest.ingest_wp_json(json.rss.channel[0]);
		//console.log("channel:", JSON.stringify(channel, null, 4));

		const table = channel.table;

		table.items = eshodinshikhi.process_items(table.items, convert_to_md);
		table.categories = eshodinshikhi.process_categories(table.categories, convert_to_md);

		//console.log("icategories:", JSON.stringify(table.categories, null, 4));

		writer.write_categories(table.categories, table.items);
		writer.write_posts(table.items.post);
		writer.write_playlists(table.items.playlists, table.items.attachment, table.items.audio);
		writer.write_audio(table.items.audio, table.items.attachment);

		/*
		console.log("categories:", table.categories.root);
		console.log("tags:", table.tags);
		console.log("terms:", table.terms);
		console.log("items:", JSON.stringify(table.items, null, 4));
		console.log("attachments:", table.attachments);
		*/
	}
	else {
		console.error(err);
	}
}

function main() {
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
		terminal: false
	});

	const xml_lines = [];

	rl.on('line', line => xml_lines.push(line));
	rl.once('close', () => {
		const xml = xml_lines.join('\n');
		parser.parseString(xml, on_json_create);
	});
}

main();

