const fs = require('fs');
const beautify = require('json-beautify');

const logging = false

function stringifyJson(obj) {
	//return JSON.stringify(obj);
	return beautify(obj, null, 2, 100);
}

function get_post_by_id(items_table, id) {
	const info = {};

	for (const type in items_table) {
		const typed_items = items_table[type];

		for (const item of typed_items) {
			if (item.id === id) {
				return {
					type: type,
					item: item
				};
			}
		}
	}

	//console.log("************* id: ", id);
}

function write_category(name, category, items_table) {
	const obj = {};

	obj['name'] = name;

	if (name !== 'root') {
		if (category.description) {
			obj['description'] = category.description;
		}

		const items = [];
		obj['items'] = items;

		for (const item of category.items) {
			const info = get_post_by_id(items_table, item);
			if (info) {
				// Some posts were removed because they were not exposed in production
				// (ie. private posts). For these posts `get_post_by_id` will return an
				// `undefined`.
				items.push({
					name: info.item.title,
					url: info.type + '_' + info.item.name + '.json',
					type: info.type,
					published_on: info.item.publication_date
				});
			}
		}
	}

	if (category.children.length > 0) {
		const subcategories = [];
		obj['subcategories'] = subcategories;

		for (const child of category.children) {
			subcategories.push({
				name: child,
				url: 'category-' + child + '.json'
			});
		}
	}

	const content = stringifyJson(obj);

	if (logging) {
		console.log(content);
		console.log('--------------------------------------------');
	}

	fs.writeFile('output/category-' + name + '.json', content, err => {
		if (err) {
			console.error(err);
		}
	});
}

function write_post(post) {
	const obj = {};
	obj['name'] = post.name;
	obj['title'] = post.title;

	const categories = [];
	obj['categories'] = categories;

	for (const category of post.categories) {
		categories.push({
			name: category,
			url: 'category-' + category + '.json'
		});
	}

	obj['content'] = post.content;
	obj['published_on'] = post.publication_date;

	const content = stringifyJson(obj);

	if (logging) {
		console.log(content);
		console.log('--------------------------------------------');
	}

	fs.writeFile('output/post-' + post.name + '.json', content, err => {
		if (err) {
			console.error(err);
		}
	});
}

function find_attachment_by_id(attachment_list, id) {
	for (const attachment of attachment_list) {
		if (attachment.id === id) {
			return attachment;
		}
	}
}

function find_audio_by_attachment_id(audio_list, attachment_id) {
	for (const audio of audio_list) {
		if (audio.children[0] === attachment_id) {
			return audio;
		}
	}
}

function write_audio(audio, attachment_list) {
	const obj = {};
	obj['name'] = audio.name;
	obj['title'] = audio.title;

	const categories = [];
	obj['categories'] = categories;

	for (const category of audio.categories) {
		categories.push({
			name: category,
			url: 'category-' + category + '.json'
		});
	}

	obj['content'] = audio.content;
	obj['excerpt'] = audio.excerpt;

	const audio_files = [];
	obj['audio_files'] = audio_files;

	for (const child of audio.children) {
		const attachment = find_attachment_by_id(attachment_list, child);
		audio_files.push({
			name: attachment.title,
			url: 'http://eshodinshikhi.com/wp-content/uploads/' + attachment.postmeta._wp_attached_file
		});
	}

	obj['published_on'] = audio.publication_date;

	const content = stringifyJson(obj);

	if (logging) {
		console.log(content);
		console.log('--------------------------------------------');
	}

	fs.writeFile('output/audio-' + audio.name + '.json', content, err => {
		if (err) {
			console.error(err);
		}
	});
}

function write_playlist(playlist, attachment_list, audio_list) {
	const obj = {};

	const lines = [];

	obj['name'] = playlist.name;
	obj['title'] = playlist.title;

	const categories = [];
	obj['categories'] = categories;

	for (const category of playlist.categories) {
		categories.push({
			name: category,
			url: 'category-' + category + '.json'
		});
	}

	obj['content'] = playlist.content;

	//console.log(attachment_list);

	const audio_attachments = [];
	const audio_posts = [];

	for (const child of playlist.children) {
		const attachment = find_attachment_by_id(attachment_list, child);
		if (logging) {
			console.log(child, attachment);
		}

		const audio = find_audio_by_attachment_id(audio_list, attachment.id);
		if (audio) {
			audio_posts.push(audio);
		}
		else {
			audio_attachments.push(attachment);
		}
	}

	if (audio_posts.length > 0) {
		const audio_posts = [];
		obj['audio_posts'] = audio_posts;

		for (const audio of audio_posts) {
			audio_posts.push({
				name: audio.title,
				url: 'audio-' + audio.name + '.json'
			});
		}
	}

	if (audio_attachments.length > 0) {
		const audio_files = [];
		obj['audio_files'] = audio_files;

		for (const attachment of audio_attachments) {
			audio_files.push({
				name: attachment.title,
				url: 'http://eshodinshikhi.com/wp-content/uploads/' + attachment.postmeta._wp_attached_file
			});
		}
	}

	obj['published_on'] = playlist.publication_date;

	const content = stringifyJson(obj);

	if (logging) {
		console.log(content);
		console.log('--------------------------------------------');
	}

	fs.writeFile('output/playlists-' + playlist.name + '.json', content, err => {
		if (err) {
			console.error(err);
		}
	});

}

function write_categories(categories_table, items_table) {
	for (const name in categories_table) {
		const category = categories_table[name];

		if (name === 'root' || category.items.length > 0) {
			write_category(name, category, items_table);
		}
	}
}

function write_posts(posts) {
	for (const post of posts) {
		write_post(post);
	}
}

function write_playlists(playlists, attachment_list, audio_list) {
	for (const playlist of playlists) {
		write_playlist(playlist, attachment_list, audio_list);
	}
}

function write_audios(audios, attachment_list) {
	for (const audio of audios) {
		write_audio(audio, attachment_list);
	}
}

module.exports = {
	write_categories: write_categories,
	write_posts: write_posts,
	write_playlists: write_playlists,
	write_audio: write_audios,
};


