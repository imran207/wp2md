const fs = require('fs');

const logging = false

function get_post_by_id(items_table, id) {
	const info = {};

	for (const type in items_table) {
		const typed_items = items_table[type];

		for (const item of typed_items) {
			if (item.id === id) {
				return {
					type: type,
					item: item
				};
			}
		}
	}

	//console.log("************* id: ", id);
}

function write_category(name, category, items_table) {
	const lines = [];

	lines.push('## Name', name, '');

	if (name !== 'root') {
		if (category.description) {
			lines.push('## Description', category.description, '');
		}

		lines.push('## Items');
		for (const item of category.items) {
			const info = get_post_by_id(items_table, item);
			if (info) {
				// Some posts were removed because they were not exposed in production
				// (ie. private posts). For these posts `get_post_by_id` will return an
				// `undefined`.
				lines.push(' - [' + info.item.title + '](' + info.type + '-' + info.item.name + '.md)', '');
			}
		}

		lines.push('');
	}

	if (category.children.length > 0) {
		lines.push('## Subcategories');
		for (const child of category.children) {
			lines.push(' - [' + child + '](category-' + child + '.md)', '');
		}
	}

	const content = lines.join("\n");

	if (logging) {
		console.log(content);
		console.log('--------------------------------------------');
	}

	fs.writeFile('output/category-' + name + '.md', content, err => {
		if (err) {
			console.error(err);
		}
	});
}

function write_post(post) {
	const lines = [];

	lines.push('## Name', post.name, '');
	lines.push('## Title', post.title, '');

	lines.push('## Categories');
	for (const category of post.categories) {
		lines.push(' - [' + category + '](category-' + category + '.md)');
	}
	lines.push('');

	lines.push('## Content');
	lines.push(post.content, '');

	lines.push('## Published on');
	lines.push(post.publication_date, '');

	const content = lines.join("\n");

	if (logging) {
		console.log(content);
		console.log('--------------------------------------------');
	}

	fs.writeFile('output/post-' + post.name + '.md', content, err => {
		if (err) {
			console.error(err);
		}
	});
}

function find_attachment_by_id(attachment_list, id) {
	for (const attachment of attachment_list) {
		if (attachment.id === id) {
			return attachment;
		}
	}
}

function find_audio_by_attachment_id(audio_list, attachment_id) {
	for (const audio of audio_list) {
		if (audio.children[0] === attachment_id) {
			return audio;
		}
	}
}

function write_audio(audio, attachment_list) {
	const lines = [];

	lines.push('## Name', audio.name, '');
	lines.push('## Title', audio.title, '');

	lines.push('## Categories');
	for (const category of audio.categories) {
		lines.push(' - [' + category + '](category-' + category + '.md)');
	}
	lines.push('');

	lines.push('## Content');
	lines.push(audio.content, '');

	lines.push('## Excerpt');
	lines.push(audio.excerpt, '');

	lines.push('## Audio file')
	for (const child of audio.children) {
		const attachment = find_attachment_by_id(attachment_list, child);
		lines.push(' - [' + attachment.title + '](http://eshodinshikhi.com/wp-content/uploads/' + attachment.postmeta._wp_attached_file + ')');
	}
	lines.push('');

	lines.push('## Published on');
	lines.push(audio.publication_date, '');

	const content = lines.join("\n");

	if (logging) {
		console.log(content);
		console.log('--------------------------------------------');
	}

	fs.writeFile('output/audio-' + audio.name + '.md', content, err => {
		if (err) {
			console.error(err);
		}
	});
}

function write_playlist(playlist, attachment_list, audio_list) {
	const lines = [];

	lines.push('## Name', playlist.name, '');
	lines.push('## Title', playlist.title, '');

	lines.push('## Categories');
	for (const category of playlist.categories) {
		lines.push(' - [' + category + '](category-' + category + '.md)');
	}
	lines.push('');

	lines.push('## Content');
	lines.push(playlist.content, '');

	//console.log(attachment_list);

	const audio_attachments = [];
	const audio_posts = [];

	for (const child of playlist.children) {
		const attachment = find_attachment_by_id(attachment_list, child);
		if (logging) {
			console.log(child, attachment);
		}

		const audio = find_audio_by_attachment_id(audio_list, attachment.id);
		if (audio) {
			audio_posts.push(audio);
		}
		else {
			audio_attachments.push(attachment);
		}
	}

	if (audio_posts.length > 0) {
		lines.push('## Audio posts')

		for (const audio of audio_posts) {
			lines.push(' - [' + audio.title + '](audio-' + audio.name + '.md)');
		}

		lines.push('');
	}

	if (audio_attachments.length > 0) {
		lines.push('## Audio files')
		for (const attachment of audio_attachments) {
			lines.push(' - [' + attachment.title + '](http://eshodinshikhi.com/wp-content/uploads/' + attachment.postmeta._wp_attached_file + ')');
		}
		lines.push('');
	}

	lines.push('## Published on');
	lines.push(playlist.publication_date, '');

	const content = lines.join("\n");

	if (logging) {
		console.log(content);
		console.log('--------------------------------------------');
	}

	fs.writeFile('output/playlists-' + playlist.name + '.md', content, err => {
		if (err) {
			console.error(err);
		}
	});

}

function write_categories(categories_table, items_table) {
	for (const name in categories_table) {
		const category = categories_table[name];

		if (name === 'root' || category.items.length > 0) {
			write_category(name, category, items_table);
		}
	}
}

function write_posts(posts) {
	for (const post of posts) {
		write_post(post);
	}
}

function write_playlists(playlists, attachment_list, audio_list) {
	for (const playlist of playlists) {
		write_playlist(playlist, attachment_list, audio_list);
	}
}

function write_audios(audios, attachment_list) {
	for (const audio of audios) {
		write_audio(audio, attachment_list);
	}
}

module.exports = {
	write_categories: write_categories,
	write_posts: write_posts,
	write_playlists: write_playlists,
	write_audio: write_audios,
};


