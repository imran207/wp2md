
/* lessons:
 *
 *    slug is a URL friendly version of a name
 */

/* questions:
 *   - do we need to preserve any comments?
 *   - there are 3 users in wp:
 *             "SupremeAdmin94"
 *             "abuubaidimran"
 *             "firstauthor"
 *         and each article is associated with one. do we need to preserve this username?
 *
 *   - there are:
 *             2 drafts
 *             484 inherit - seems to be attachments
 *             50 pending - seems like questions
 *             10 private - ???
 *             426 publish - published posts
 *
 *        do we need the drafts, questions, private posts?
 */

const node_html_markdown = require('node-html-markdown');


function find_by_id(table, id) {
	for (const item of table) {
		if (item.id === id)
			return item;
	}
}

function get_all_items(items) {
	const all_items = [];

	for (const type in items) {
		const typed_items = items[type];

		for (const item of typed_items) {
			all_items.push(item);
		}
	}

	return all_items;
}

function remove_private_items(item_table) {
	// Eliminate private posts
	const items = {};

	for (const type in item_table) {
		const typed_items = item_table[type];
		const new_typed_items = [];

		for (const item of typed_items) {
			const status = item.status;

			if (status !== 'private' && status !== 'draft') {
				new_typed_items.push(item);
			}
		}

		if (new_typed_items.length > 0) {
			items[type] = new_typed_items;
		}
	}

	return items
}

function mask_rooted_categories(category_table) {
	// mark any categories as 'rooted' if it has some posts associated with it
	// when a category is marked as rooted, all of its ancestor nodes are also
	// marked rooted.
	for (const nicename in category_table) {
		const category = category_table[nicename];

		if (category.items.length > 0) {
			p = category;

			while (p) {
				category.is_rooted = true;
				p = p.parent;
			}
		}
	}
}

function mark_rooted_posts(item_table) {
	// Mark posts that are rooted
	let unmarked_items = get_all_items(item_table);
	const marked_items = new Set();
	let more_work = true;

	while (more_work) {
		const new_unmarked_items = [];
		more_work = false;

		for (const unmarked_item of unmarked_items) {
			if (unmarked_item.categories.length > 0) {
					marked_items.add(unmarked_item.id);
					unmarked_item.is_rooted = true;
					more_work = true;
			}
			else {
				let found = false;

				for (const p of unmarked_item.parent) {
					if (marked_items.has(p)) {
						found = true;
						break;
					}
				}

				if (found) {
					marked_items.add(unmarked_item.id);
					unmarked_item.is_rooted = true;
					more_work = true;
				}
				else {
					new_unmarked_items.push(unmarked_item);
				}

			}
		}

		unmarked_items = new_unmarked_items;
	}
}

function flatten_salutations(html) {
	const re = /(font-family\s*:\s*['"]?Arabic Symbols['"]?[^>]*[>])([^<]*)([<]\/span\>)/g;

	html = html.replace(re, (_, p1, p2, p3) => {
		p2 = p2.replace(/./g, ch => {
			// The Arabic characters are represented in terms of unicode values because they
			// appear in reverse when viewed in a plain text editor (due to the language being RTL
			// and the editor assuming it's LTR). The text was converted using
			// https://r12a.github.io/app-conversion/

			switch (ch) {
				case '0':
					// Subhanahu Wa Ta’yala
					// سُبْحَانَهُ وَتَعَالَىٰ
					return "(\u{633}\u{64F}\u{628}\u{652}\u{62D}\u{64E}\u{627}\u{646}\u{64E}\u{647}\u{64F} \u{648}\u{64E}\u{62A}\u{64E}\u{639}\u{64E}\u{627}\u{644}\u{64E}\u{649}\u{670})";

				case '1':
					// Sallallaahu ‘Alyhi Wa Sallaam
					// ﷺ
					// https://www.compart.com/en/unicode/U+FDFA
					return "(\u{FDFA})";

				case '2':
					// Sallallaahu ‘Alyhi Wa Sallaam (possible duplicate of '1')
					return "(unknown text)";

				case '3':
					// Radiyallahu ‘Amhu
					// رضي الله عنه
					return "(\u{631}\u{636}\u{64A} \u{627}\u{644}\u{644}\u{647} \u{639}\u{646}\u{647})";

				case '4':
					// Radiyallahu ‘Amhum (couldn't find transliteration)
					return "(unknown text)";

				case '5':
					// 'Alyhimussalam
					// ﷷ    ﷵ
					// https://www.compart.com/en/unicode/U+FDF7
					// https://www.compart.com/en/unicode/U+FDF5
					return "(\u{FDF7} \u{FDF5})";

				case '7':
					// ‘Azza Wa Jallaa
					// عَزَّ وَجَلَّ
					return "(\u{639}\u{64E}\u{632}\u{64E}\u{651} \u{648}\u{64E}\u{62C}\u{64E}\u{644}\u{64E}\u{651})";

				case '8':
					// Jalla Zalaluhu
					// ﷻ
					// https://www.compart.com/en/unicode/U+FDFB
					return "(\u{FDFB})";

				case 'b':
					// Jalla Wa ‘Ala (couldn't find transliteration)
					return "(unknown text)";

				case 'c':
					// Hafizhullah
					// حفيظ الله
					return "(\u{62D}\u{641}\u{64A}\u{638} \u{627}\u{644}\u{644}\u{647})";

				case 'd':
					// Hafizahallaah (couldn't find transliteration)
					return "(unknown text)";

				case 'e':
					// Radiyallaahu ‘Anhu
					// رَضِيَ ٱللَّٰهُ عَنْهُ
					// https://en.wikipedia.org/wiki/Islamic_honorifics
					return "(\u{631}\u{64E}\u{636}\u{650}\u{64A}\u{64E} \u{671}\u{644}\u{644}\u{64E}\u{651}\u{670}\u{647}\u{64F} \u{639}\u{64E}\u{646}\u{652}\u{647}\u{64F})";

				case 'f':
					// Radiyallaahu ‘Anha
					// رَضِيَ ٱللَّٰهُ عَنْهَا
					// https://en.wikipedia.org/wiki/Islamic_honorifics
					return "(\u{631}\u{64E}\u{636}\u{650}\u{64A}\u{64E} \u{671}\u{644}\u{644}\u{64E}\u{651}\u{670}\u{647}\u{64F} \u{639}\u{64E}\u{646}\u{652}\u{647}\u{64E}\u{627})";

				case 'g':
					// Radiyallaahu ‘Anhum (modified by me so needs extra checking)
					// رَضِيَ ٱللَّٰهُ عَنْهُمْ
					// https://en.wikipedia.org/wiki/Islamic_honorifics
					return "(\u{631}\u{64E}\u{636}\u{650}\u{64A}\u{64E} \u{671}\u{644}\u{644}\u{64E}\u{651}\u{670}\u{647}\u{64F} \u{639}\u{64E}\u{646}\u{652}\u{647}\u{64F}\u{645}\u{652})";

				case 'h':
					// Radiyallaahu ‘Anhuma (modified by me so needs extra checking)
					// رَضِيَ ٱللَّٰهُ عَنْهُمْا
					// https://en.wikipedia.org/wiki/Islamic_honorifics
					return "(\u{631}\u{64E}\u{636}\u{650}\u{64A}\u{64E} \u{671}\u{644}\u{644}\u{64E}\u{651}\u{670}\u{647}\u{64F} \u{639}\u{64E}\u{646}\u{652}\u{647}\u{64F}\u{645}\u{652}\u{627})";

				case 'i':
					// Radiyallaahu Anhunna (coudn't find transliteration)
					return "(unknown text)";

				case 'j':
					// ‘Alyhi salatu Assalaam (needs checking)
					// عَلَيْهِ ٱلصَّلَاةُ وَٱلسَّلَامُ
					// https://en.wikipedia.org/wiki/Islamic_honorifics
					return "(\u{639}\u{64E}\u{644}\u{64E}\u{64A}\u{652}\u{647}\u{650} \u{671}\u{644}\u{635}\u{64E}\u{651}\u{644}\u{64E}\u{627}\u{629}\u{64F} \u{648}\u{64E}\u{671}\u{644}\u{633}\u{64E}\u{651}\u{644}\u{64E}\u{627}\u{645}\u{64F})";

				case 'k':
					// Alyhissallam
					// ﷷ    ﷸ
					// https://www.compart.com/en/unicode/U+FDF7
					// https://www.compart.com/en/unicode/U+FDF8
					return "(\u{FDF7} \u{FDF8})";

				case 'm':
					// ‘Alayhimus Salaam (couldn't find transliteration)
					return "(unknown text)";

				case 'n':
					// Alayhimas Salaam (couldn't find transliteration)
					return "(unknown text)";

				case 'o':
					// Rahimahullah
					// رَحِمَهُ ٱللَّٰهُ
					// https://en.wikipedia.org/wiki/Rahimahullah
					return "(\u{631}\u{64E}\u{62D}\u{650}\u{645}\u{64E}\u{647}\u{64F} \u{671}\u{644}\u{644}\u{64E}\u{651}\u{670}\u{647}\u{64F})";

				case 'p':
					// Rahimahumullah (couldn't find transliteration)
					return "(unknown text)";

				case 'q':
					// Rahimahumallaah (couldn't find transliteration)
					return "(unknown text)";

				case 't':
					// Bismillaah Hir Rahmanir Rahim
					// ﷽
					// https://www.compart.com/en/unicode/U+FDFD
					return "(\u{FDFD})";

				default:
					return ch;
			}
		});

		return p1 + p2 + p3;
	});

	return html;
}

function htmls_to_markdowns(item, convert_to_md) {
	if (item.excerpt) {
		item.excerpt = flatten_salutations(item.excerpt);
		if (convert_to_md) {
			item.excerpt = node_html_markdown.NodeHtmlMarkdown.translate(item.excerpt, {});
		}
	}

	if (item.content) {
		item.content = flatten_salutations(item.content);
		if (convert_to_md) {
			item.content = node_html_markdown.NodeHtmlMarkdown.translate(item.content, {});
		}
	}
}

function process_audio(audio_table, attachment_table, convert_to_md) {
	for (const audio of audio_table) {
		const id = parseInt(audio.postmeta.audio_file);

		htmls_to_markdowns(audio, convert_to_md);

		if (id) {
			audio.children.splice(0, Infinity, id);

			// Mark the attachment item as rooted
			const attachment = find_by_id(attachment_table, id);
			attachment.parent.add(audio.id);
		}
		else {
			audio.children.splice(0, Infinity);
		}
	}
}

function process_playlists(playlist_table, attachment_table, convert_to_md) {
	for (const playlist of playlist_table) {
		const match = playlist.content.match("\\[playlist ids=\\\"([0-9,]+)\\\"\\]");
		playlist.content = playlist.content.replace(match[0], "");

		const ids = match[1].split(',');
		playlist.children.splice(0, Infinity, ...ids);
		playlist.children = playlist.children.map(x => parseInt(x));

		htmls_to_markdowns(playlist, convert_to_md);

		for (const id of ids) {
			const id_num = parseInt(id);

			// Link attachment with audio file within the playlist
			const attachment = find_by_id(attachment_table, id_num);
			attachment.parent.add(playlist.id);
		}
	}
}

function remove_unrooted_posts(item_table) {
	// Eliminate unrooted posts
	const items = {};

	for (const type in item_table) {
		const typed_items = item_table[type];
		const new_typed_items = [];

		for (const item of typed_items) {
			if (item.is_rooted) {
				new_typed_items.push(item);
			}
		}

		if (new_typed_items.length > 0) {
			items[type] = new_typed_items;
		}
	}

	return items;
}

function remove_unrooted_categories(category_table) {
	// Eliminate unrooted categories
	const new_table = {};

	for (const nicename in category_table) {
		const category = category_table[nicename];

		if (category.is_rooted) {
			new_table[nicename] = category;
		}
	}

	return new_table;
}

function process_ebooks(ebook_table, attachment_table, convert_to_md) {
	for (const ebook of ebook_table) {
		const id = parseInt(ebook.postmeta.ebook_link);
		ebook.children.splice(0, Infinity, id);

		htmls_to_markdowns(ebook, convert_to_md);

		// Link attachment with ebook
		const attachment = find_by_id(attachment_table, id);
		attachment.parent.add(ebook.id);
	}
}

function process_posts(post_table, convert_to_md) {
	for (const post of post_table) {
		const id = post.id;

		for (const parent of post.parent) {
			const parent_item = find_by_id(parent);
			parent_item.children.add(id);
		}

		htmls_to_markdowns(post, convert_to_md);
	}
}

function process_items(item_table, convert_to_md) {
	item_table = remove_private_items(item_table);

	process_audio(item_table.audio, item_table.attachment, convert_to_md);
	process_playlists(item_table.playlists, item_table.attachment, convert_to_md);
	process_ebooks(item_table.ebooks, item_table.attachment, convert_to_md);
	process_posts(item_table.post, convert_to_md);

	mark_rooted_posts(item_table);
	item_table = remove_unrooted_posts(item_table);

	return item_table;

	/* post typed:
	 *   attachment (references to mp3, pdf)
	 *   audio -> attachment (take only the latest one)
	 *   ebooks -> attachment
	 *   playlists
	 *   post
	 *   questions
	 *
	 *
	 */
}

function link_categories(category_table) {
	// Add references to child category from parent category
	const root_category = { is_rooted: true, items:[], children: [] };

	for (const category_name in category_table) {
		const category = category_table[category_name];

		if (category.parent) {
			category_table[category.parent].children.push(category_name);
		}
		else {
			root_category.children.push(category_name);
		}
	}

	category_table.root = root_category;
}

function process_categories(category_table, convert_to_md) {
	mask_rooted_categories(category_table);
	category_table = remove_unrooted_categories(category_table);

	link_categories(category_table);

	for (const category_name in category_table) {
		const category = category_table[category_name];

		if (category.description) {
			category.description = flatten_salutations(category.description);
			if (convert_to_md) {
				category.description = node_html_markdown.NodeHtmlMarkdown.translate(category.description, {});
			}
		}
	}

	return category_table;
}

module.exports = {
	process_items: process_items,
	process_categories: process_categories
};

/*


*/
