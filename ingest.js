
function process_category(categories, obj) {
	const nicename = obj['wp:category_nicename'][0];
	const category = {
		"id": parseInt(obj['wp:term_id'][0]),
		"parent": obj['wp:category_parent'][0],
		"name": obj['wp:cat_name'][0],
		"description": obj['wp:category_description'],
		"items": [],
		"children": [],
		"is_rooted": false
	};

	if (category.description)
		category.description = category.description[0];

	Object.seal(category);
	categories[nicename] = category;
}

function process_tag(tags, obj) {
	const slug = obj['wp:tag_slug'][0];
	const tag = {
		"id": parseInt(obj['wp:term_id'][0]),
		"name": obj['wp:tag_name'][0],
		"items": []
	};

	Object.seal(tag);
	tags[slug] = tag;
}

function process_term(terms, obj) {
	const slug = obj['wp:term_slug'][0];
	const term = {
		"id": parseInt(obj['wp:term_id'][0]),
		"taxonomy": obj['wp:term_taxonomy'][0],
		"name": obj['wp:term_name'][0],
		"items": []
	};

	Object.seal(term);
	terms[slug] = term;
}

function process_item(table, attachment_set, obj) {
	const type = obj['wp:post_type'][0];
	let typed_items = table.items[type];

	if (!typed_items) {
		typed_items = [];
		table.items[type] = typed_items;
	}

	const item = {
		"title": obj.title[0],
		"link": obj.link[0],
		"publication_date": obj.pubDate[0],
		"content": obj['content:encoded'][0],
		"excerpt": obj['excerpt:encoded'][0],
		"id": parseInt(obj['wp:post_id'][0]),
		"post_date_gmt": obj['wp:post_date_gmt'][0],
		"name": obj['wp:post_name'][0],
		"status": obj['wp:status'][0],
		"parent": new Set(),
		"attachment": -1,
		"tags": [],
		"categories": [],
		"terms": [],
		"children": [],
		"is_rooted": false
	}

	let attachment = obj['wp:attachment_url']

	if (attachment) {
		attachment = item.attachment[0];
		let index = attachment_set[attachment];

		if (index === undefined) {
			index = table.attachments.length;
			attachment_set[attachment] = index;
			table.attachments.push(attachment);
		}

		item.attachment = index;
	}

	let parent = parseInt(obj['wp:post_parent'][0]);
	if (parent) {
		item.parent.add(parent);
	}

	const categories = obj.category;
	if (categories) {
		// Add links between the `item` table and the
		// (`tags`, `categories` and `terms`) tables
		for (const category of categories) {
			const domain = category.$.domain;
			const nicename = category.$.nicename;

			if (domain === 'post_tag') {
				item.tags.push(nicename);
				table.tags[nicename].items.push(item.id);
			}
			else if (domain === 'category') {
				item.categories.push(nicename);
				table.categories[nicename].items.push(item.id);
			}
			else {
				item.terms.push(nicename);
				table.terms[nicename].items.push(item.id);
			}
		}
	}

	// Import the post metadata
	item.postmeta = {};
	const postmeta_pairs = obj['wp:postmeta'];
	for (const pair of postmeta_pairs) {
		item.postmeta[pair['wp:meta_key']] = pair['wp:meta_value'][0];
	}


	// skip: guid
	// skip dc:creator
	// skip wp:comment_status
	// skip wp:ping_status
	// skip wp:menu_order
	// skip wp:comment

	Object.seal(item);
	typed_items.push(item);
}

function process_channel(obj) {
	const table = {
		categories: {},
		tags: {},
		terms: {},
		items: {},
		attachments: []
	};

	const channel = {
		title: obj.title[0],
		description: obj.description[0],
		last_update: obj.pubDate[0],
		language: obj.language[0],
		table: table
	};

	// ignoring wp:author


	// Process categories
	for (const category of obj['wp:category']) {
		process_category(table.categories, category);
	}

	// Process tags
	for (const tag of obj['wp:tag']) {
		process_tag(table.tags, tag);
	}

	// Process terms
	for (const term of obj['wp:term']) {
		process_term(table.terms, term);
	}

	// Process items
	const attachment_set = {};
	for (const item_obj of obj['item']) {
		process_item(table, attachment_set, item_obj);
	}

	console.log("Link: " + obj.link[0]);

	return channel;
}

module.exports = {
	ingest_wp_json: function(obj) {
		return process_channel(obj);
	}
};


